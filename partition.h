int partition(char array[], int low, int high, char pivot)
 {   
   int i = low;
   for(int j = low; j<high; j++)
   {
        if(array[j] <pivot) 
            {   
                swap(array[i], array[j]);
                i++;
            }
        else if(array[j] == pivot) 
            {    
             swap(array[j], array[high]);
             j--;
            }
   }
   swap(array[i], array[high]);
   return i;    
}