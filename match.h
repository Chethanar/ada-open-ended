void nutAndBoltMatch(char nuts[], char bolts[], int low, int high)
{
   if(low < high)
   {
        int pivotLoc = partition(nuts, low, high, bolts[high]);   
        partition(bolts, low, high, nuts[pivotLoc]);    
        nutAndBoltMatch(nuts, bolts, low, pivotLoc - 1);
        nutAndBoltMatch(nuts, bolts, pivotLoc+1, high);
   }
}